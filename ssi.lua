local SSI = {}

local tinsert = table.insert

function SSI:new(o)
    o = o or {}
    setmetatable(o, self)
    self.__index = self

    o.spellIds = {}
    o.canSave = {}
    o.updateCanSave = {}

    return o
end

function SSI:Init()
    Apollo.RegisterAddon(self)
end

function SSI:OnLoad()
	self.xmlDoc = XmlDoc.CreateFromFile("ssi.xml")
	self.xmlDoc:RegisterCallback("OnDocLoaded", self)
end

function SSI:OnDocLoaded()
	if self.xmlDoc ~= nil and self.xmlDoc:IsLoaded() then
	    self.wndMain = Apollo.LoadForm(self.xmlDoc, "BaseForm", nil, self)
		if self.wndMain == nil then
			Apollo.AddAddonErrorText(self, "Could not load the main window for some reason.")
			return
		end

        self.output = self.wndMain:FindChild("Output")
        self.grid = self.wndMain:FindChild("Spells")

	    self.wndMain:Show(false, true)

		Apollo.RegisterSlashCommand("ssi", "OnCommand", self)
	end
end

function SSI:OnCommand()
    local spellIds = self.spellIds
    local canSave = self.canSave
    local abilityCount = 0
    local abilities = {}
    local abilityIdToSpellId = {}

    local grid = {{}, {}, {}}

    local exclude = {["Flame Burst"]=true}
    local abilityTags = {
        Spell.CodeEnumSpellTag.Assault,
        Spell.CodeEnumSpellTag.Support,
        --Spell.CodeEnumSpellTag.Utility
    }

    for idx, tag in pairs(abilityTags) do
        a = AbilityBook.GetAbilitiesList(tag)
        for _, ability in pairs(a) do
            abilityCount = abilityCount + 1
            if ability.tTiers[1].splObject:GetCooldownTime() > 0 then
                local name = ability.strName
                if not exclude[name] then
                    tinsert(grid[idx], name)
                    abilities[name] = {}
                    spellIds[name] = spellIds[name] or {}
                    canSave[name] = true
                    abilityIdToSpellId[ability.nId] = ability.tTiers[1].splObject:GetId() -- cache the base spell id to use later to get spell info
                end
            end
        end
    end
    assert(abilityCount == 20)

    -- build the grid
    local rows = 0
    for i = 1, 3 do
        local count = #grid[i]
        if count > rows then
            rows = count
        end
    end

    local spellGridLoc = {}
    for i = 1, rows do
        self.grid:AddRow("")
        for j = 1, 3 do
            local name = grid[j][i]
            if name then
                spellGridLoc[name] = {i, j}
                self.grid:SetCellText(i, j, name)
                self.grid:SetCellImage(i, j, "CRB_Basekit:kitIcon_Holo_XDisabled")
            end
        end
    end

    self.gridSpells = grid
    self.spellGridLoc = spellGridLoc

    local MAX_SPELL_ID = 200000
    for i=1, MAX_SPELL_ID do
        spell = GameLib.GetSpell(i)
        name = spell:GetName()
        if abilities[name] then
            tinsert(abilities[name], spell)
        end
    end

    self.abilityIdToSpellId = abilityIdToSpellId
    self.abilities = abilities

    local lasIds = ActionSetLib.GetCurrentActionSet()
    for i = 1, 8 do
        AbilityBook.UpdateSpellTier(lasIds[i], 1)
    end
    ActionSetLib.RequestActionSetChanges(lasIds)

    Apollo.RegisterEventHandler("VarChange_FrameCount", "OnUpdate", self)
	self.wndMain:Invoke()
end

function SSI:OnUpdate()
    local canSave = self.canSave
    local updateCanSave = self.updateCanSave

    if not GameLib.IsSpellSurgeActive() then
        local lasIds = ActionSetLib.GetCurrentActionSet()
        local abilityIdToSpellId = self.abilityIdToSpellId
        local abilities = self.abilities
        local spellIds = self.spellIds

        for i = 1, 10 do
            local id = abilityIdToSpellId[lasIds[i]]  -- id will be nil for empty las slots (lasIds contains a 0)
            if id then
                local ability = AbilityBook.GetAbilityInfo(id)
                if ability then  -- path action and gadget don't pass this test
                    -- scan the ability for cooldowns
                    local name = ability.strName
                    local spells = abilities[name]
                    for j = 1, #spells do
                        local spell = spells[j]
                        local tier = spell:GetTier()
                        local id = spell:GetId()
                        if spell:GetCooldownRemaining() > 0 then
                            if canSave[name] then
                                updateCanSave[id] = true
                                canSave[name] = false
                            end
                        elseif updateCanSave[id] then
                            updateCanSave[id] = nil
                            canSave[name] = true
                        end
                    end
                end
            end
        end

        return
    end

    local lasIds = ActionSetLib.GetCurrentActionSet()
    local abilityIdToSpellId = self.abilityIdToSpellId
    local abilities = self.abilities
    local spellIds = self.spellIds
    local activeSpells = {}

    for i = 1, 10 do
        local id = abilityIdToSpellId[lasIds[i]]  -- id will be nil for empty las slots (lasIds contains a 0)
        if id then
            local ability = AbilityBook.GetAbilityInfo(id)
            if ability then  -- path action and gadget don't pass this test
                -- scan the ability for cooldowns
                local name = ability.strName
                local spells = abilities[name]
                for j = 1, #spells do
                    local spell = spells[j]
                    local tier = spell:GetTier()
                    local id = spell:GetId()
                    if spell:GetCooldownRemaining() > 0 then
                        if canSave[name] then
                            spellIds[name][tier] = id
                            activeSpells[lasIds[i]] = {name=name, tier=tier}

                            local complete = true
                            for checkTier=1, 9 do
                                if not spellIds[name][checkTier] then
                                    complete = false
                                    break
                                end
                            end

                            if complete then
                                local row, col = unpack(self.spellGridLoc[name])
                                self.grid:SetCellImage(row, col, "CRB_Basekit:kitIcon_Complete")
                            end
                        end
                    elseif updateCanSave[id] then
                        SendVarToRover("UpdateCanSave", {id=id, name=name}, 0)
                        updateCanSave[id] = nil
                        canSave[name] = true
                    end
                end
            end
        end
    end

    local line = "%s,%d,%d\n"
    local output = ""
    for name, tiers in pairs(spellIds) do
        for tier, id in pairs(tiers) do
            output = output..line:format(name, tier, id)
        end
    end

    if output ~= self.output:GetText() then
        self.output:SetText(output)
    end

    local update = false
    for lasId, info in pairs(activeSpells) do
        newTier = (info.tier % 9) + 1
        AbilityBook.UpdateSpellTier(lasId, newTier)
        update = true
        if newTier == 1 then
            canSave[info.name] = false
        end
    end

    if update then
        ActionSetLib.RequestActionSetChanges(lasIds)
    end
end

function SSI:OnClose()
    Apollo.RemoveEventHandler("VarChange_FrameCount", self)
    self.wndMain:Close()
end

function SSI:OnGenerateTooltip(_, _, _, row, col)
    row, col = row + 1, col + 1

    local spellName = self.gridSpells[col][row]
    if spellName then
        local xml = XmlDoc.new()
        local text = "Need tiers: "
        for tier = 1, 9 do
            if not self.spellIds[spellName][tier] then
                text = text..tier..", "
            end
        end
        text = text:sub(0, -3)
        xml:AddLine(spellName)
        xml:AddLine(text)
        self.grid:SetTooltipDoc(xml)
    else
        self.grid:SetTooltipDoc(nil)
    end
end

ssi = SSI:new()
ssi:Init()
